const fs = require('fs');
const jsonfile = require('jsonfile');

var board = {};

box = function(){
  return {
    isVisible : false,
    value     : 0,
    row       : 0,
    col       : 0,
    build : function (row, col, val) {
      this.row   = row;
      this.col   = col;
      this.value = val;
      return this;
    },
    setVisible : function(){
      this.isVisible = true;
      return this;
    },
    increment : function(){
      this.value++;
      return this;
    },
    print : function(){
      return JSON.stringify(this);
      //
      let retVal = "<div>";
      retVal    += "<form action=\"clear\" method='post' id=\""+this.row+"_"+this.col+"\"></form>";
      retVal    += "<input type=\"hidden\" form=\""+this.row+"_"+this.col+"\" name=\"row\" value=\""+this.row+"\">";
      retVal    += "<input type=\"hidden\" form=\""+this.row+"_"+this.col+"\" name=\"col\" value=\""+this.col+"\">";
      retVal    += "<input type=\"hidden\" form=\""+this.row+"_"+this.col+"\" name=\"value\" value=\""+this.value+"\">"
      if (this.isVisible) {
        retVal += "<input form=\""+this.row+"_"+this.col+"\" type=\"submit\" value=\"" + this.value + "\">";
      } else {
        retVal += "<input form=\""+this.row+"_"+this.col+"\" type=\"submit\" value=\" X \">";
      }
      retVal += "</div>"
      console.log (retVal);
      return retVal;
    }
  };
};

board = function(id){
  return {
    id   : id,
    file : "./"+id+".json",
    get  : function (){
      //console.log ("Get: " + this.id);
      if (!fs.existsSync(this.file)) {
        console.log ("Created empty file");
        fs.writeFileSync(this.file, '[]');
      }
      const gameBoard = require (this.file);
    //  const gameBoard_json = fs.readFileSync(this.file, 'utf8');
      //const gameBoard      = JSON.parse(gameBoard_json);
    //  console.log (JSON.stringify(gameBoard));
      return gameBoard;
    },
    build : function (size){
      // TODO: Set the id to be a time based hash
      console.log ("Building Board: " + this.id);

      // Create a new playing Board
      let gameBoard = this.get(this.file);

      if (gameBoard.length === 0) {

      // Fill in the board with '0'
      for (let i = 0; i < size; i++){
        let row = new Array ();
        for (let j = 0; j < size; j++){
          let newBox = new box();
          newBox.build(i, j, 0);
          row.push(newBox);
        }
        gameBoard.push(row);
      }

      // Randomly generate |size| Bombs and scatter in the board
      for (let i = 0; i < size; i++){
        let row = Math.floor(Math.random() * size);
        let col = Math.floor(Math.random() * size);
        // TODO: Prevent collision of already placed bomb
        gameBoard[row][col].value = 9;
      }

      // Sliding window, generate values for each box
      for (let i = 0; i < gameBoard.length; i++){
        for (let j = 0; j < gameBoard[i].length; j++){
          // If a Bomb is found, check up, down, left, right
          if (gameBoard[i][j].value === 9){
          // TODO: fix this logic
            // Check upper left, up, upper right
            if (i > 0) {
              for (let k = j - 1; k <= j + 1; k++) {
                if (k >= 0 && k <= gameBoard[i - 1].length - 1 && gameBoard[i - 1][k].value != 9) {
                    gameBoard[i - 1][k].increment();
                }
              }
            }
            // Check left
            if (j > 0 && gameBoard[i][j - 1].value != 9) {
              gameBoard[i][j - 1].increment();
            }
            // Check right
            if (j < gameBoard[i].length-1 && gameBoard[i][j + 1].value != 9) {
              gameBoard[i][j + 1].increment();
            }
            // Check lower left, down, lower right
            if (i < gameBoard.length - 1) {
              for (let k = j - 1; k <= j + 1; k++) {
                if (k >=0 && k <= gameBoard[i + 1].length - 1 && gameBoard[i + 1][k].value != 9) {
                  gameBoard[i + 1][k].increment();
                }
              }
            }
          }
        }
      }

      // Store the new board in a JSONDB
      jsonfile.writeFile(this.file, gameBoard)
        .then(function(res){
          return this;
        })
        .catch(function(err){
          console.log(err);
          return this;
        });
    }
      // Return the Board
      return this;
    },
    clear : function (row, col){
      let gameBoard = this.get();

      // Set the Box as Visible
      gameBoard[row][col].isVisible = true;

      if (gameBoard[row][col].value == 0) {
        for (let i = row - 1; i <= row + 1; i++) {
          if (0 > i || i > gameBoard.length - 1) { continue; }
          for (let j = col - 1; j <= col + 1; j++) {
            if (0 > j || j > gameBoard[i].length - 1) { continue; }
            if (gameBoard[i][j] === undefined) { continue; }
            if (gameBoard[i][j].isVisible == true) { continue; }
            this.clear(i,j);
          }
        }
      }

      // Store the new board in a JSONDB
      jsonfile.writeFile(this.file, gameBoard)
        .then(function(res){
          return this;
        })
        .catch(function(err){
          console.log(err);
          return this;
        });
      return this;
    },
    clearBox : function (row, col){
      let gameBoard = this.get();
      let ans = new Array();

      // Set the Box as Visible
      gameBoard[row][col].isVisible = true;
      return gameBoard[row][col].value;

//      if (gameBoard[row][col].value == 0) {
//        for (let i = row - 1; i <= row + 1; i++) {
//          if (0 > i || i > gameBoard.length - 1) { continue; }
//          for (let j = col - 1; j <= col + 1; j++) {
//            if (0 > j || j > gameBoard[i].length - 1) { continue; }
//            if (gameBoard[i][j] === undefined) { continue; }
//            if (gameBoard[i][j].isVisible == true) { continue; }
//            ans.push(this.clearBox(i,j));
//            ans.push(gameBoard[i][j].value);
//         }
//        }
//      }
//
//      return ans;
    },
    print : function (){
      console.log ("Printing Board: " + this.id);
      let gameBoard = this.get();
      // Create JSON object to be returned
      let retVal;
      retVal = {
        // Save the Board Id
        id : this.id,
        // Create an array to store the Board
        board : new Array()
      };

      // Loop over all rows
      for (let i = 0; i < gameBoard.length; i++) {
        // Create a new JSON object for the row
        let row = new Array ();
        // Loop over all columns in the row
        for (let j = 0; j < gameBoard[i].length; j++) {
          if (gameBoard[i][j].isVisible) {
            // Create a new box with visible value
            let newBox = new box();
            newBox.build(i, j, gameBoard[i][j].value);
            newBox.setVisible();
            row.push(newBox);
          } else {
            // Create a new box with unknown value
            let newBox = new box();
            newBox.build(i, j, '-');
            row.push(newBox);
          }
        } // Loop over columns
        retVal.board.push(row);
      } // Loop over rows
      return JSON.stringify(retVal);
    }
  };
};

module.exports = board;